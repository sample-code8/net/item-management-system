﻿using CsvHelper.Configuration.Attributes;

namespace CsvImportingApp
{
    /// <summary>
    /// 商品CSVデータ（1行）を格納するクラス。
    /// </summary>
    public class ItemCsvEntity
    {
        /// <summary>商品CD</summary>
        [Name("商品CD")]
        public string ItemCode { get; set; }

        /// <summary>商品名</summary>
        [Name("商品名")]
        public string ItemName { get; set; }

        /// <summary>カテゴリ</summary>
        [Name("カテゴリ")]
        public string Category { get; set; }

        /// <summary>単価[円]</summary>
        [Name("単価")]
        public int UnitPrice { get; set; }
    }
}