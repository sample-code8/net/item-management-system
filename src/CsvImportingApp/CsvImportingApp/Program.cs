﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

namespace CsvImportingApp
{
    /// <summary>
    /// プログラムクラス
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            List<ItemCsvEntity> csvItems = ReadCsv($"{Directory.GetCurrentDirectory()}\\Data\\ItemList.csv");

            int saveCount = SaveCsv(csvItems);

            Console.WriteLine($"保存件数: {saveCount}");
        }

        private static List<ItemCsvEntity> ReadCsv(string path)
        {
            // CSVファイル読み込みの設定
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ",",
                TrimOptions = TrimOptions.Trim,
            };

            // CSVファイルデータ読み込み
            List<ItemCsvEntity> csvItems = new List<ItemCsvEntity>();
            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader, config))
            {
                var records = csv.GetRecords<ItemCsvEntity>();

                foreach (var record in records)
                {
                    Console.WriteLine("-----------------------------");
                    Console.WriteLine($"商品CD: {record.ItemCode}");
                    Console.WriteLine($"商品名: {record.ItemName}");
                    Console.WriteLine($"カテゴリ: {record.Category}");
                    Console.WriteLine($"単価: {record.UnitPrice}");
                    csvItems.Add(record);
                }
            }

            return csvItems;
        }

        private static int SaveCsv(List<ItemCsvEntity> csvItems)
        {
            int saveCount = 0;

            using (ItemDbContext db = new ItemDbContext())
            {
                foreach (ItemCsvEntity csvItem in csvItems)
                {
                    var dbCategory = db.CATEGORIES
                        .FirstOrDefault(c => c.CATEGORY_NAME == csvItem.Category);
                    if (dbCategory == null)
                    {
                        dbCategory = new CATEGORY()
                        {
                            CATEGORY_NAME = csvItem.Category,
                        };
                        db.CATEGORIES.Add(dbCategory);
                    }

                    var dbItem = db.ITEMS
                        .FirstOrDefault(i => i.ITEM_CODE == csvItem.ItemCode);
                    if (dbItem == null)
                    {
                        dbItem = new ITEM()
                        {
                            ITEM_CODE = csvItem.ItemCode,
                            ITEM_NAME = csvItem.ItemName,
                            CATEGORY_ID = dbCategory.CATEGORY_ID,
                            UNIT_PRICE = csvItem.UnitPrice,
                        };

                        db.ITEMS.Add(dbItem);
                    }
                    else
                    {
                        dbItem.ITEM_NAME = csvItem.ItemName;
                        dbItem.CATEGORY_ID = dbCategory.CATEGORY_ID;
                        dbItem.UNIT_PRICE = csvItem.UnitPrice;
                    }

                    saveCount += db.SaveChanges();
                }

                return saveCount;
            }
        }
    }
}