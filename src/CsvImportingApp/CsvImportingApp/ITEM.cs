//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはテンプレートから生成されました。
//
//     このファイルを手動で変更すると、アプリケーションで予期しない動作が発生する可能性があります。
//     このファイルに対する手動の変更は、コードが再生成されると上書きされます。
// </auto-generated>
//------------------------------------------------------------------------------

namespace CsvImportingApp
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// 商品
    /// </summary>
    public partial class ITEM
    {
        /// <summary>商品ID</summary>
        public int ITEM_ID { get; set; }
        /// <summary>商品CD</summary>
        public string ITEM_CODE { get; set; }
        /// <summary>商品名</summary>
        public string ITEM_NAME { get; set; }
        /// <summary>単価</summary>
        public int UNIT_PRICE { get; set; }
        /// <summary>カテゴリID</summary>
        public int CATEGORY_ID { get; set; }
    
        /// <summary>カテゴリ情報</summary>
        public virtual CATEGORY CATEGORY { get; set; }
    }
}
